    // Fonction pour afficher la boîte de dialogue de confirmation
    function confirmDeleteCategory(categoryId) {
        if (confirm("Êtes-vous sûr de vouloir supprimer cette catégorie et toutes ses sous-catégories ?")) {
            // Utilisez AJAX pour effectuer la suppression
            jQuery.ajax({
                type: 'POST',
                url: admin_vars.ajax_url,
                data: {
                    action: 'delete_category_action',
                    category_id: categoryId,
                    nonce: admin_vars.nonce,
                },
                error: function (response) {
                    alert("Une erreur est survenue lors de la suppression de la catégorie");
                },
                success: function (response) {
                    // Rafraîchissez la page après la suppression
                    location.reload();
                },
            });
        }
    }


 // Fonction pour afficher la boîte de dialogue de confirmation
    function confirmDeleteLink(linkId) {
        if (confirm("Êtes-vous sûr de vouloir supprimer ce lien ?")) {
            // Utilisez AJAX pour effectuer la suppression
            jQuery.ajax({
                type: 'POST',
                url: admin_vars.ajax_url,
                data: {
                    action: 'delete_link_action',
                    link_id: linkId,
                    nonce: admin_vars.nonce,
                },
                error: function (response) {
                    alert("Une erreur est survenue lors de la suppression du lien");
                },
                success: function (response) {
                    // Rafraîchissez la page après la suppression
                    location.reload();
                },
            });
        }
    }