<?php

require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';

// Créez une classe qui étend WP_List_Table
class Category_List_Table extends \WP_List_Table {
    
    // Constructeur de la classe
    public function __construct() {
        parent::__construct(array(
            'singular' => 'catégorie',     // Nom singulier de l'élément dans la liste
            'plural'   => 'catégories',   // Nom pluriel de l'élément dans la liste
            'ajax'     => true            // Prendre en charge les requêtes AJAX (false pour cet exemple)
        ));
    }

    // Définissez les colonnes de la table
    public function get_columns() {
        return array(
            //'cb'         => '<input type="checkbox" />', // Case à cocher pour la sélection en masse
            'id'       => 'Id de la catégorie',
            'name'       => 'Nom de la catégorie',
			'image'		=> 'Image',
            'delete'       => "Suppression"
            //'description'=> 'Description',
            //'count'      => 'Nombre de liens', // Vous pouvez personnaliser les colonnes selon vos besoins
        );
    }


    // Définissez les éléments à afficher dans la table
    public function prepare_items() {
        $categories = get_root_categories();

        $data = array();
        foreach ($categories as $category) {
            $data[] = array(
                'id'          => $category->category_id,
                'name'        => $category->category_name,
                'parent'       => $category->category_parent,
				'image' 	=>	$category->image_url
            );

            $children = get_child_categories($category->category_id);
            foreach ($children as $child) {
                $data[] = array(
                    'id'          => $child->category_id,
                    'name'        => "--------- ".$child->category_name,
                    'parent'       => $child->category_parent,
					'image' 	=>	$category->image_url
                );
            }
        }

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = array($columns, $hidden, $sortable);

        $this->items = $data;
    }

    // Affiche le contenu de chaque colonne pour chaque élément
    public function column_default($item, $column_name) {
        switch ($column_name) {
            case 'name':
                return $item["name"];
            case 'id':
                return $item["id"];
            case 'parent':
                return $item["parent"];
			case 'image' :
				return sprintf(
					'<img src="%s" width="100"/>',
					$item['image']
				);
            case 'delete':
                return sprintf(
                    '<button class="button" onclick="confirmDeleteCategory(%d)">Supprimer</button>',
                    $item['id']
                );
            default:
                return print_r($item, true); // En cas de colonne inconnue, affiche l'ensemble des données de l'élément
        }
    }

    // Ajoute une colonne de case à cocher pour la sélection en masse
    public function column_cb($item) {
        return '<input type="checkbox" name="category[]" value="' . esc_attr($item['id']) . '" />';
    }

    // Définissez quelles colonnes sont triables
    public function get_sortable_columns() {
        return array(
            // 'id' => array('id', false),
            // 'name' => array('name', false),
        );
    }

    public function single_row( $item ) {
        if (null != $item["parent"]){
            echo '<tr class="child-category">';
        }
        else {

            echo '<tr class="root-category">';
        }
        $this->single_row_columns( $item );
        echo '</tr>';
    }
}

