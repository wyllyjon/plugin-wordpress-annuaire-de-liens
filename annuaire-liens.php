<?php
/*
Plugin Name: Annuaire de liens
Description: Gère un annuaire de liens.
Version: 1.1
Author: contact@jowe.fr
*/

define('ANNUAIRE_LIENS_VERSION', '1.2');

function annuaire_liens_update() {
    $current_version = get_option('annuaire_liens_version');

    if ($current_version != ANNUAIRE_LIENS_VERSION) {

        // Mettre à jour la version stockée dans les options
        update_option('annuaire_liens_version', ANNUAIRE_LIENS_VERSION);
    }
}
add_action('init', 'annuaire_liens_update');



require_once plugin_dir_path(__FILE__) . 'models.php';
require_once plugin_dir_path(__FILE__) . 'categories.php';
require_once plugin_dir_path(__FILE__) . 'Category_List_Table.php';
require_once plugin_dir_path(__FILE__) . 'Link_List_Table.php';
require_once plugin_dir_path(__FILE__) . 'liens.php';

require_once plugin_dir_path(__FILE__) . 'templates/template-liens.php';

register_activation_hook(__FILE__, 'activation_function');
register_deactivation_hook(__FILE__, 'deactivation_function');

add_action('plugins_loaded', 'updateDB');

function updateDB() {
	// Vérifiez si une mise à jour est nécessaire
	$current_version = get_option('annuaire_liens_version');

	if ($current_version === false || version_compare($current_version, '1.2', '<')) {
		global $wpdb;
		$charset_collate = $wpdb->get_charset_collate();

		$sql = "ALTER TABLE " . $wpdb->prefix . "annuaire_categories ADD COLUMN image_url VARCHAR(255) DEFAULT NULL;";
		$wpdb->query($sql);

		// Mettez à jour la version du plugin
		update_option('annuaire_liens_version', '1.2');
	}
}

function activation_function() {
    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();

    // Table des catégories
    $table_categories = $wpdb->prefix . 'annuaire_categories';
    $sql_categories = "CREATE TABLE $table_categories (
        category_id INT NOT NULL AUTO_INCREMENT,
        category_name VARCHAR(255) NOT NULL,
        category_parent INT NULL,
        PRIMARY KEY  (category_id),
        FOREIGN KEY (category_parent) REFERENCES $table_categories(category_id)
    ) $charset_collate;";
    require_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($sql_categories);


    // Table des liens
    $table_links = $wpdb->prefix . 'annuaire_links';
    $sql_links = "CREATE TABLE $table_links (
        link_id INT NOT NULL AUTO_INCREMENT,
        title VARCHAR(255) NOT NULL,
        url VARCHAR(255) NOT NULL,
        description TEXT,
        image_url VARCHAR(255),
        location_lat DECIMAL(10, 8),
        location_lng DECIMAL(11, 8),
        PRIMARY KEY  (link_id)
    ) $charset_collate;";
    dbDelta($sql_links);

    // Table de liaison entre les liens et les catégories
    $table_link_category = $wpdb->prefix . 'annuaire_link_category';
    $sql_link_category = "CREATE TABLE $table_link_category (
        link_id INT NOT NULL,
        category_id INT NOT NULL,
        PRIMARY KEY  (link_id, category_id),
        FOREIGN KEY (link_id) REFERENCES $table_links(link_id),
        FOREIGN KEY (category_id) REFERENCES $table_categories(category_id)
    ) $charset_collate;";
    dbDelta($sql_link_category);
}

function deactivation_function() {
  	global $wpdb;

    // Table des catégories
    $table_categories = $wpdb->prefix . 'annuaire_categories';
    $wpdb->query("DROP TABLE IF EXISTS $table_categories");

    // Table des liens
    $table_links = $wpdb->prefix . 'annuaire_links';
    $wpdb->query("DROP TABLE IF EXISTS $table_links");

    // Table de liaison entre les liens et les catégories
    $table_link_category = $wpdb->prefix . 'annuaire_link_category';
    $wpdb->query("DROP TABLE IF EXISTS $table_link_category");
}


add_action('admin_menu', 'annuaire_plugin_menu');

function annuaire_plugin_menu() {
	add_menu_page('Gestion de l\'annuaire', 'Annuaire', 'manage_options', 'annuaire-gestion', 'annuaire_gestion_page');
    add_submenu_page('annuaire-gestion', 'Gérer les Catégories', 'Catégories', 'manage_options', 'annuaire-gestion-categories', 'annuaire_gestion_categories_page');
    add_submenu_page('annuaire-gestion', 'Gérer les Liens', 'Liens', 'manage_options', 'annuaire-gestion-liens', 'annuaire_gestion_liens_page');
    add_submenu_page('annuaire-gestion', 'Paramètres du Plugin', 'Paramètres', 'manage_options', 'annuaire-parametres', 'annuaire_parametres_page');
    add_submenu_page('annuaire-gestion', 'Ajouter un Lien', 'Ajouter un Lien', 'manage_options', 'annuaire-ajout-lien', 'annuaire_ajout_lien_page');
}

function annuaire_gestion_page(){
	echo '<div class="wrap"><h1>Plugin Annuaire de liens</h1></div>';
}

function annuaire_parametres_page() {
    // Code pour la page de paramètres du plugin
    echo '<div class="wrap"><h1>Paramètres du Plugin</h1></div>';
}

function enqueue_admin_styles() {
    wp_enqueue_style('admin-styles', plugin_dir_url(__FILE__) . 'admin-styles.css');
}

add_action('admin_enqueue_scripts', 'enqueue_admin_styles');


function enqueue_admin_scripts() {
    wp_enqueue_script('admin-scripts', plugin_dir_url(__FILE__) . 'admin-scripts/admin-script.js', array('jquery'), '1.0', true);
    // Passez les données nécessaires au script JavaScript
    wp_localize_script('admin-scripts', 'admin_vars', array(
        'ajax_url' => admin_url('admin-ajax.php'),
        'nonce'    => wp_create_nonce('annuaire_delete_nonce'),
    ));
}

add_action('admin_enqueue_scripts', 'enqueue_admin_scripts');


function upload_file ($field_name) {
	if (isset($_FILES[$field_name]) && $_FILES[$field_name]['error'] == 0) {
		$file = $_FILES[$field_name];

		// Vérifiez si le fichier est une image
		$image_info = getimagesize($file['tmp_name']);
		if ($image_info === false) {
			// Le fichier n'est pas une image, renvoyez une erreur ou faites quelque chose d'autre
			return null;
		}

		// Limitez le type de fichiers acceptés (ici, seulement les images)
		$allowed_types = array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG);
		if (!in_array($image_info[2], $allowed_types)) {
			// Le type de fichier n'est pas autorisé, renvoyez une erreur ou faites quelque chose d'autre
			return null;
		}

		// Générez un nom unique pour le fichier
		$file_name = wp_unique_filename(wp_upload_dir()['path'], $file['name']);

		// Déplacez le fichier téléchargé vers le répertoire d'uploads
		move_uploaded_file($file['tmp_name'], wp_upload_dir()['path'] . '/' . $file_name);

		// Retournez l'URL complète du fichier téléchargé
		return wp_upload_dir()['url'] . '/' . $file_name;
	}

	return null;
}