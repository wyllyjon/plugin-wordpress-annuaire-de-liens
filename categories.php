<?php

function get_annuaire_categories() {
     global $wpdb;

    $table_categories = $wpdb->prefix . 'annuaire_categories';
    $query = "SELECT * FROM $table_categories";
    return $wpdb->get_results($query);
}

function get_root_categories() {
     global $wpdb;

    $table_categories = $wpdb->prefix . 'annuaire_categories';
    $query = "SELECT * FROM $table_categories WHERE category_parent IS NULL";
    return $wpdb->get_results($query);
}

function get_parent_from_cat($cat) {
	global $wpdb;

	$table_categories = $wpdb->prefix . 'annuaire_categories';
	$query = "SELECT category_parent FROM $table_categories WHERE category_id=$cat";
	return $wpdb->get_var($query);
}

function get_child_categories($parent) {
    global $wpdb;

    $table_categories = $wpdb->prefix . 'annuaire_categories';
    $query = "SELECT * FROM $table_categories WHERE category_parent=$parent";
    return $wpdb->get_results($query);
}

function display_categories_page() {
    $category_list_table = new Category_List_Table();
    $category_list_table->prepare_items();

    echo '<div class="wrap">';
    echo '<h1>Liste des Catégories</h1>';
    
    // Affichez le message de suppression si nécessaire
    if (isset($_GET['deleted']) && $_GET['deleted'] == 'true') {
        echo '<div class="updated"><p>Catégorie supprimée avec succès.</p></div>';
    }

    // Affichez la table
    $category_list_table->display();

    echo '</div>';
}

function annuaire_gestion_categories_page() {
      // Vérifiez si le formulaire d'ajout de catégorie est soumis
    if (isset($_POST['addCategorie'])) {
        // Récupérez les données du formulaire
        $category_name = sanitize_text_field($_POST['category_name']);
        $category_parent = isset($_POST['category_parent']) ? intval($_POST['category_parent']) : null;
		$image_url = upload_file('category_image');
        // Validez les données ici (si nécessaire)

        // Ajoutez la nouvelle catégorie à la base de données
        annuaire_add_category($category_name, $category_parent, $image_url);

        echo '<div class="updated"><p>Nouvelle catégorie ajoutée avec succès.</p></div>';
    }


    // echo '<div class="wrap"><h1>Gérer les Catégories</h1>';

    display_categories_page();

    // Afficher la liste des catégories existantes
    // $categories = get_root_categories();
    
    // if ($categories) {
    //     echo '<ul>';
    //     foreach ($categories as $category) {
    //         echo '<li>' . esc_html($category->category_name) . '</li>';

    //         $children = get_child_categories($category->category_id);
    //         if (sizeof($children) > 0)
    //         {
    //             echo "<ul>";
    //             foreach ($children as $child) {
    //                 echo '<li>----' . esc_html($child->category_name) . '</li>';
    //             }
    //             echo "</ul>";
    //         }
    //     }
    //     echo '</ul>';
    // } else {
    //     echo '<p>Aucune catégorie trouvée.</p>';
    // }

    // Ajouter un lien pour créer une nouvelle catégorie
    // echo '</div>';

    annuaire_ajout_categorie_page();
}

// Nouvelle page d'ajout de catégories
function annuaire_ajout_categorie_page() {
    echo '<div class="wrap"><h1>Ajouter une nouvelle catégorie</h1>';

    echo '<div id="col-left">';
    // Affichez le formulaire d'ajout de catégorie
    echo '<form class="form-wrap"  method="post" action="" enctype="multipart/form-data">';
    echo '<div class="form-field">';
    echo '<label for="category_name">Nom de la catégorie:</label>';
    echo '<input type="text" name="category_name" required>';
    echo '</div>';
    
    echo '<div class="form-field">';
    echo '<label for="category_parent">Catégorie parente:</label>';
    echo '<select name="category_parent">';
    echo '<option>Aucune</option>'; // Option pour aucune catégorie parente
    // Obtenez la liste des catégories existantes pour la sélection
    $categories = get_root_categories();
    foreach ($categories as $category) {
        echo '<option value="' . esc_attr($category->category_id) . '">' . esc_html($category->category_name) . '</option>';
    }
    echo '</select>';

	echo '<label for="category_image">Image de la catégorie:</label>';
	echo '<input type="file" name="category_image" id="category_image" />';

    echo '</div>';

    echo '<input type="submit" class="button button-primary" name="addCategorie" value="Ajouter Catégorie">';
    echo '</form>';

    echo '</div>';
    echo '</div>';
}

function annuaire_add_category($category_name, $category_parent = null, $image_url = null) {
    global $wpdb;

    $table_categories = $wpdb->prefix . 'annuaire_categories';

    $data = array('category_name' => $category_name);

    if ($category_parent != null)
    {
        $data['category_parent'] = $category_parent;
    }

	if ($image_url != null)
	{
		$data['image_url'] = $image_url;
	}


    $wpdb->insert(
        $table_categories,
        $data
    );
}

function delete_category_action() {
    check_ajax_referer('annuaire_delete_nonce', 'nonce');

    $category_id = intval($_POST['category_id']);

    if (delete_category_and_subcategories($category_id))
    {
        wp_send_json_success();
    }
    else
    {
        wp_send_json_error();
    }

    die(); // Cela est nécessaire pour terminer correctement la requête AJAX
}

add_action('wp_ajax_delete_category_action', 'delete_category_action');

function delete_category_and_subcategories($category_id) {
    global $wpdb;

    // Obtenez toutes les sous-catégories de la catégorie spécifiée
    $subcategories = get_child_categories($category_id);

    try {
        // Démarrer une transaction pour assurer une suppression atomique
        $wpdb->query('START TRANSACTION');
        $wpdb->query('SET FOREIGN_KEY_CHECKS=0');

        // Supprimez les sous-catégories
        foreach ($subcategories as $subcategory) {
            $wpdb->delete($wpdb->prefix . 'annuaire_categories', array('category_id' => $subcategory->category_id));
        }

        // Supprimez la catégorie principale
        $wpdb->delete($wpdb->prefix . 'annuaire_categories', array('category_id' => $category_id));

        // Validez la transaction

        $wpdb->query('COMMIT');
        $wpdb->query('SET FOREIGN_KEY_CHECKS=1');

        return true; // La suppression a réussi
    } catch (Exception $e) {
        error_log(print_r($e));
        // En cas d'erreur, annulez la transaction
        $wpdb->query('ROLLBACK');
        $wpdb->query('SET FOREIGN_KEY_CHECKS=1');

        return false; // La suppression a échoué
    }
}
