<?php
/*
Template Name: Affichage des listes de lien
*/

add_shortcode('afficher_liens', 'shortcode_liste_liens');

require_once plugin_dir_path(__FILE__) . '../categories.php';
require_once plugin_dir_path(__FILE__) . '../liens.php';

function shortcode_liste_liens() {
    ob_start(); // Démarre la temporisation de sortie
    afficher_template_liens(); // Utilise la fonction précédemment définie pour afficher le template
    return ob_get_clean(); // Retourne le contenu de la temporisation de sortie
}

function afficher_template_liens() {

    $premier_niveau = true;
    $catBack = null;

    if (isset($_GET['back']))
	{
		// On revient vers la catégorie au-dessus et on affiche la catégorie "back" ainsi que toutes les catégories ayant le même parent
		$catBackId = $_GET['back'];
		$catBack = get_parent_from_cat($catBackId);
	}

    if (isset($_GET['parent']) || $catBack != null) {
        $catParent = isset($_GET['parent']) ? $_GET['parent'] : $catBack ;
        $categories = get_child_categories($catParent);
        $links = get_links_by_category ($catParent);
        $premier_niveau = false;
    }
    else {
        $categories = get_root_categories();
    }

    if (!$premier_niveau) {
    	echo '<div><a href="?back='.$catParent.'"> Retour à la catégorie parente </a><br/></div>';
	}

    // Affichage des catégories
    if (sizeof($categories) > 0)
    {
        if ($categories) {
            echo '<ul>';
            foreach ($categories as $category) {
                echo '<li>';
                echo '<a href="?parent=' . $category->category_id. '">'.$category->category_name.'</a>';
                echo '</li>';
            }
            echo '</ul>';
        } else {
            if($premier_niveau)
                echo '<p>Aucune catégorie trouvée.</p>';
        }
    }

    // Affichage des liens
    if (isset($links) && sizeof($links) > 0)
    {
       foreach ($links as $link) {
            echo '<li>';
            echo $link->title.' : '.$link->url;
            echo '</li>';
        } 
    }
    else {
        if(!$premier_niveau)
        echo "Aucun lien dans cette catégorie";
    }
}

function lienRetour() {

}