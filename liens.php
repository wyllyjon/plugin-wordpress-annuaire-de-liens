<?php

function get_annuaire_links() {
    global $wpdb;

    $table_links = $wpdb->prefix . 'annuaire_links';
    $query = "SELECT * FROM $table_links";
    return $wpdb->get_results($query);
}

function get_links_by_category($category_id) {
    global $wpdb;

    $table_links = $wpdb->prefix . 'annuaire_links';
    $table_link_category = $wpdb->prefix . 'annuaire_link_category';

    $query = $wpdb->prepare(
        "SELECT links.*
        FROM $table_links links
        INNER JOIN $table_link_category link_cat ON links.link_id = link_cat.link_id
        WHERE link_cat.category_id = %d",
        $category_id
    );

    return $wpdb->get_results($query);
}

function annuaire_get_link_categories($link_id) {
    global $wpdb;

    $table_link_category = $wpdb->prefix . 'annuaire_link_category';
    $table_categories = $wpdb->prefix . 'annuaire_categories';

    $query = $wpdb->prepare(
        "SELECT cat.category_name
        FROM $table_link_category lc
        INNER JOIN $table_categories cat ON lc.category_id = cat.category_id
        WHERE lc.link_id = %d",
        $link_id
    );

    return $wpdb->get_results($query);
}

function annuaire_gestion_liens_page() {
     echo '<div class="wrap"><h1>Gérer les Liens</h1>';

    // Afficher la liste des liens existants
    display_links_page();

    // Ajouter un lien pour créer un nouveau lien
    echo '<p><a href="' . admin_url('admin.php?page=annuaire-ajout-lien') . '">Ajouter un nouveau lien</a></p>';
    echo '</div>';
}


function display_links_page() {
    $link_list_table = new Link_List_Table();
    $link_list_table->prepare_items();

    echo '<div class="wrap">';
    echo '<h1>Liste des Liens</h1>';

    // Affichez la table
    $link_list_table->display();

    echo '</div>';
}

// Nouvelle page d'ajout de liens
function annuaire_ajout_lien_page() {
     echo '<div class="wrap"><h1>Ajouter un nouveau lien</h1>';

    // Vérifiez si le formulaire est soumis
    if (isset($_POST['submit'])) {
        // Récupérez les données du formulaire
        $title = sanitize_text_field($_POST['title']);
        $url = esc_url($_POST['url']);
        $description = sanitize_textarea_field($_POST['description']);
		$image_url = upload_file('link_image');
        $location_lat = isset($_POST['location_lat']) ? floatval($_POST['location_lat']) : null;
        $location_lng = isset($_POST['location_lng']) ? floatval($_POST['location_lng']) : null;
        $categories = isset($_POST['categories']) ? $_POST['categories'] : array();

        // Validez les données ici (si nécessaire)

        // Ajoutez le nouveau lien à la base de données
        $link_id = annuaire_add_link($title, $url, $description, $image_url, $location_lat, $location_lng);

        // Associez le lien aux catégories sélectionnées
        annuaire_associate_link_categories($link_id, $categories);

        echo '<div class="updated"><p>Nouveau lien ajouté avec succès.</p></div>';
    }

    // Affichez le formulaire d'ajout de lien avec les catégories
    echo '<div id="col-left">';
    echo '<form class="form-wrap" method="post" action=""  enctype="multipart/form-data">';
    echo '<div class="form-field">';
    echo '<label for="title">Titre du lien:</label>';
    echo '<input type="text" name="title" required>';
    echo '</div>';
    
    echo '<div class="form-field">';
    echo '<label for="url">URL:</label>';
    echo '<input type="url" name="url" required><br/><br/>';
    echo '</div>';

    echo '<div class="form-field">';
    echo '<label for="description">Description:</label>';
    echo '<textarea name="description"></textarea><br/><br/>';
    echo '</div>';

    echo '<div class="form-field">';
	echo '<label for="link_image">Image du lien:</label>';
	echo '<input type="file" name="link_image" id="link_image" />';
    echo '</div>';

    echo '<div class="form-field">';
    echo '<label for="location_lat">Latitude (si applicable):</label>';
    echo '<input type="text" name="location_lat"><br/><br/>';
    echo '</div>';

    echo '<div class="form-field">';
    echo '<label for="location_lng">Longitude (si applicable):</label>';
    echo '<input type="text" name="location_lng"><br/><br/>';
    echo '</div>';

    // Affichez la liste des catégories
    $categories = get_annuaire_categories();
    if ($categories) {
        echo '<div class="form-field">';
        echo '<label for="categories">Catégories:</label>';
        echo '<select name="categories[]" multiple>';
        foreach ($categories as $category) {
            echo '<option value="' . esc_attr($category->category_id) . '">' . esc_html($category->category_name) . '</option>';
        }
        echo '</select>';
        echo '</div>';
    }

    echo '<input type="submit" name="submit" class="button button-primary" value="Ajouter Lien">';
    echo '</form>';

    echo '</div>';
    echo '</div>';
}

function annuaire_add_link($title, $url, $description, $image_url = null, $location_lat = null, $location_lng = null) {
    global $wpdb;

    $table_links = $wpdb->prefix . 'annuaire_links';

    $wpdb->insert(
        $table_links,
        array(
            'title' => $title,
            'url' => $url,
            'description' => $description,
            'image_url' => $image_url,
            'location_lat' => $location_lat,
            'location_lng' => $location_lng,
        )
    );

     // Retournez l'ID du lien ajouté
    return $wpdb->insert_id;

}

function annuaire_associate_link_categories($link_id, $categories) {
    global $wpdb;

    $table_link_category = $wpdb->prefix . 'annuaire_link_category';

    foreach ($categories as $category_id) {
        $wpdb->insert(
            $table_link_category,
            array(
                'link_id' => $link_id,
                'category_id' => $category_id,
            )
        );
    }
}

function delete_link_action() {
    check_ajax_referer('annuaire_delete_nonce', 'nonce');

    $link_id = intval($_POST['link_id']);

    if (delete_link($link_id))
    {
        wp_send_json_success();
    }
    else
    {
        wp_send_json_error();
    }

    die(); // Cela est nécessaire pour terminer correctement la requête AJAX
}

add_action('wp_ajax_delete_link_action', 'delete_link_action');

function delete_link($link_id) {
    global $wpdb;

    try {
        // Démarrer une transaction pour assurer une suppression atomique
        $wpdb->query('START TRANSACTION');

        // Supprimez les liaisons entre le lien et les catégories
        $wpdb->delete($wpdb->prefix . 'annuaire_link_category', array('link_id' => $link_id));

        // Supprimez le lien de la table des liens
        $wpdb->delete($wpdb->prefix . 'annuaire_links', array('link_id' => $link_id));

        // Validez la transaction
        $wpdb->query('COMMIT');

        return true; // La suppression a réussi
    } catch (Exception $e) {
        // En cas d'erreur, annulez la transaction
        $wpdb->query('ROLLBACK');
        error_log(print_r($e));

        return false; // La suppression a échoué
    }
}



