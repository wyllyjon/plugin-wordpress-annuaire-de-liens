<?php

require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';

// Créez une classe qui étend WP_List_Table
class Link_List_Table extends \WP_List_Table {
    
    // Constructeur de la classe
    public function __construct() {
        parent::__construct(array(
            'singular' => 'lien',     // Nom singulier de l'élément dans la liste
            'plural'   => 'liens',   // Nom pluriel de l'élément dans la liste
            'ajax'     => true            // Prendre en charge les requêtes AJAX (false pour cet exemple)
        ));
    }

    // Définissez les colonnes de la table
    public function get_columns() {
        return array(
            //'cb'         => '<input type="checkbox" />', // Case à cocher pour la sélection en masse
            'id'       => 'Id',
            'title'       => 'Titre',
            'url'       => 'URL',
			'image'		=> 'Image',
            'delete'       => "Suppression"
        );
    }


    // Définissez les éléments à afficher dans la table
    public function prepare_items() {
        $links = get_annuaire_links();

        $data = array();
        foreach ($links as $link) {
            $data[] = array(
                'id'          => $link->link_id,
                'title'        => $link->title,
                'url'       => $link->url,
				'image' 	=>	$link->image_url
            );
        }

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = array($columns, $hidden, $sortable);

        $this->items = $data;
    }

    // Affiche le contenu de chaque colonne pour chaque élément
    public function column_default($item, $column_name) {
        switch ($column_name) {
            case 'title':
                return $item["title"];
            case 'id':
                return $item["id"];
            case 'url':
                return $item["url"];
			case 'image' :
				return sprintf(
					'<img src="%s" width="100"/>',
					$item['image']
				);
            case 'delete':
                return sprintf(
                    '<button class="button" onclick="confirmDeleteLink(%d)">Supprimer</button>',
                    $item['id']
                );
            default:
                return print_r($item, true); // En cas de colonne inconnue, affiche l'ensemble des données de l'élément
        }
    }

    // Définissez quelles colonnes sont triables
    public function get_sortable_columns() {
        return array(
            // 'id' => array('id', false),
            // 'name' => array('name', false),
        );
    }
}

