<?php
// models.php

class Annuaire_Category {
    public $category_id;
    public $category_name;
    public $category_parent;
	public $image_url;
}

class Annuaire_Link {
    public $link_id;
    public $title;
    public $url;
    public $description;
    public $image_url;
    public $location_lat;
    public $location_lng;
}

class Annuaire_Link_Category {
    public $link_id;
    public $category_id;
}